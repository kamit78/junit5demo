package Velocity;

import BuildInExtensions.ParameterResolverDemo;
import Utilities.ThreadController;
import org.junit.jupiter.api.extension.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class VelocityExecution implements BeforeAllCallback, ParameterResolver, AfterAllCallback, TestWatcher {

    private static ExtensionContext.Namespace HANDLES = ExtensionContext.Namespace.GLOBAL;
    private static Map<String, RemoteWebDriver> driverMap = new ConcurrentHashMap<>();
    private static ExtensionContext.Namespace NAMESPACE = ExtensionContext.Namespace.GLOBAL;

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception {

        System.out.println("Run before all test cases...............");
        System.out.println(Thread.currentThread().getName());
        System.out.println("################################################");
        System.out.println(extensionContext.getStore(NAMESPACE).get("remotes"));
        System.out.println("###################################################");

        Optional nodes = Optional.ofNullable(extensionContext.getStore(NAMESPACE).get("remotes"));

        System.out.println(nodes.isPresent());

        if (!nodes.isPresent()) {
            System.out.println("Creating webdriver remote controls...........");
            ThreadController threadController = new ThreadController();
            System.out.println("Start creating threads" + Thread.currentThread().getName());
            List<Future<RemoteWebDriver>> remotes = threadController.spawnNodeThreads(2, "chrome");
//            System.out.println("Number of remotes are " + remotes.size());
            extensionContext.getStore(NAMESPACE).put("remotes", remotes);
            System.out.println("Ending creating of thread" + Thread.currentThread().getName());
        } else
            System.out.println("WEbdriver remoetes aer already exist");

    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {

//        System.out.println("#E@@@#@#@#@#@support Parameter");
//        System.out.println(parameterContext.getParameter().getType());
//        System.out.println("#E@@@#@#@#@#@support Parameter");

        if (parameterContext.getParameter().getType().equals(RemoteWebDriver.class))
            return true;
        else
            return false;

//        System.out.println(Thread.currentThread().getName());
//        return true;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {

//        System.out.println(Thread.currentThread().getName());
        List<Future<RemoteWebDriver>> nodes = (ArrayList)extensionContext.getStore(NAMESPACE).get("remotes");
        System.out.println("Current context of test method is " + extensionContext.getUniqueId());
        Collections.shuffle(nodes);
        try {
            driverMap.put(extensionContext.getUniqueId(), nodes.get(0).get());
//            System.out.println("##############################3");
//            System.out.println(nodes.get(0).get());
//            System.out.println("%%%%%#$#$$#$#$#$#$#$#$");
            extensionContext.getStore(HANDLES).put("driverMap", driverMap);
            return nodes.get(0).get();
        } catch (InterruptedException | ExecutionException e) {

            System.out.println("Exception occurred........" );
            return null;
        }
    }

    @Override
    public void afterAll(ExtensionContext extensionContext) throws Exception {
        List<Future<RemoteWebDriver>> nodes = (ArrayList)extensionContext.getStore(NAMESPACE).get("remotes");
        nodes.forEach(node -> {
            try {
                node.get().quit();
            }catch (InterruptedException | ExecutionException e){
                System.out.println("Exception in quiting drivers");
            }
        });
    }

    @Override
    public void testDisabled(ExtensionContext extensionContext, Optional<String> optional) {


    }

    @Override
    public void testSuccessful(ExtensionContext extensionContext) {

        ConcurrentHashMap<String, RemoteWebDriver> drivers = (ConcurrentHashMap<String, RemoteWebDriver>) extensionContext.getStore(HANDLES).get("driverMap");

        System.out.println(drivers.size());

        System.out.println(drivers.get(extensionContext.getUniqueId()).getTitle());

    }

    @Override
    public void testAborted(ExtensionContext extensionContext, Throwable throwable) {



    }

    @Override
    public void testFailed(ExtensionContext extensionContext, Throwable throwable) {

        System.out.println("Test Failed........");
        ConcurrentHashMap<String, RemoteWebDriver> drivers = (ConcurrentHashMap<String, RemoteWebDriver>) extensionContext.getStore(HANDLES).get("driverMap");
        ScreenshotManager.makeScreenshot(drivers.get(extensionContext.getUniqueId()));

    }
}
