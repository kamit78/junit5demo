package PlayGround;

import Velocity.TestExtension;
import Velocity.VelocityExecution;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.openqa.selenium.remote.RemoteWebDriver;

//@ExtendWith(VelocityExecution.class)
class Script2Parallel {

    @Tag("maven")
    @Test
    void test01(RemoteWebDriver driver){

        System.out.println("test 1 of script 2 ..." + Thread.currentThread().getName());
    }

//    @Tag("maven")
    @Test
    void test02(){

        System.out.println("test 2 of script 2 ...");
        System.out.println(Thread.currentThread().getName());
    }
}
