package examples;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assumptions.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Amit.Kumar2 on 2/22/2019.
 */


class AssumptionDemo {

    @Tag("Assume")
    @Test
    void testAssumption(){

        System.out.println("before assumption");
        assumeTrue("amit".contains("z"));
        System.out.println("after assume");

    }

    @Tag("maven")
    @Test
    void testAa(){

        System.out.println("Test 333");

    }


}
