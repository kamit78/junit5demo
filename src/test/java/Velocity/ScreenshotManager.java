package Velocity;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;

public class ScreenshotManager {

    public static  void makeScreenshot(RemoteWebDriver driver){

        File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshot, new File("D:\\screenshotw2323223.png"));
        }catch (IOException e){
            System.out.println("Exception");
        }
    }

}
