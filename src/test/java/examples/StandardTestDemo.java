package examples;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

/**
 * Created by Amit.Kumar2 on 2/20/2019.
 */

@DisplayName("A special test case")
public class StandardTestDemo {

    @BeforeAll
    static void initAll() {

        System.out.println("Before All test cases........");
    }

    @BeforeEach
    void init() {

    }

    @DisplayName("Test case is succedded ...")
    @Test
    void succeedingTest() {

    }

    @Test
    void failingTest() {
        fail("a failing test");
    }

    @Test
    @Disabled("for demonstration purposes")
    void skippedTest() {
        // not executed
    }

    @Tag("maven")
    @Test
    void abortedTest() {
        assumeTrue("abc".contains("Z"));
        fail("test should have been aborted");
    }

    @AfterEach
    void tearDown() {
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("After All test cases........");
    }

}
