package Sources;

public enum EnumSources {

    HIGH(1),
    MEDIUM(2),
    LOW(2);

    private final int value;

    EnumSources(int value) {
        this.value = value;

    }
    public int getValue() {

        return this.value;
    }


}
