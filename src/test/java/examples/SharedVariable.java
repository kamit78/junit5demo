package examples;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SharedVariable {

    String shared = "hello";

    @Test
    void test01(){

        System.out.println(shared);
        shared = "changed";
    }

    @Test
    void test02(){

        System.out.println(shared);
    }
}
