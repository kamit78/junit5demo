package BuildInExtensions;

import Velocity.VelocityExecution;
import org.junit.jupiter.api.extension.*;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.lang.reflect.Parameter;
import java.util.Optional;

public class TestWatcherDemo implements TestWatcher {

    @Override
    public void testDisabled(ExtensionContext extensionContext, Optional<String> optional) {


    }

    @Override
    public void testSuccessful(ExtensionContext extensionContext) {

        System.out.println("Test successful " + extensionContext.getDisplayName());

    }

    @Override
    public void testAborted(ExtensionContext extensionContext, Throwable throwable) {




    }

    @Override
    public void testFailed(ExtensionContext extensionContext, Throwable throwable) {



    }


}
