package examples;

import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.DynamicTest;
import static org.junit.jupiter.api.Assertions.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class TestFactoryDemo {


    @TestFactory
    Stream<DynamicTest> test01() {

        String[][] arr = {{"hello", "pello"},{"j", "p"}};

        return Stream.of(arr).map(pha -> DynamicTest.dynamicTest("test" + pha[0], () -> {

            System.out.println(pha[0]);
            System.out.println(pha[1]);

        }));

    }

    // Test that all even numbers upto 10 are divisible by 2
    @TestFactory
    Stream<DynamicTest> test02(){

        return IntStream.iterate(0,n-> n+2).limit(10).mapToObj(n -> DynamicTest.dynamicTest("test" + n, ()-> assertTrue(n%2 ==0)));
    }
}

