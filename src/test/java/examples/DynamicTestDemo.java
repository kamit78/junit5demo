package examples;

import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.ThrowingConsumer;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.*;

class DynamicTestDemo {


    @TestFactory
    Collection<DynamicTest> dynamicTestsFromCollection(){

        return Arrays.asList(dynamicTest("First dynamic Test", ()-> assertTrue(true)),
                dynamicTest("Second dynamic test", ()-> assertTrue(true)));
    }



    @TestFactory
    Stream<DynamicTest> generateRandomNumberOfTests(){

        Iterator<Integer> inputGenerator = new Iterator<Integer>() {

            Random random = new Random();
            int current;


            @Override
            public boolean hasNext() {
                current = random.nextInt(100);
                return (current % 7) != 0;
            }

            @Override
            public Integer next() {
                return current;
            }
        };

        ThrowingConsumer<Integer> testExecutor = (input) -> assertTrue(input % 7 != 0);

        Function<Integer, String> displayNameGenerator = (input) -> "input:" + input;

        return DynamicTest.stream(inputGenerator, displayNameGenerator, testExecutor);
    }



    @TestFactory
    DynamicNode dynamicNodeSingleTest() {
        return dynamicTest("'pop' is a palindrome", () -> assertTrue(true));
    }

}
