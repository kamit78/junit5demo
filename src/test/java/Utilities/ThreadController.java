package Utilities;

//mport base.TestBase;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.Remote;
import java.util.*;
import java.util.concurrent.*;
//import base.TestBase.*;
import org.openqa.selenium.remote.RemoteWebElement;

/**
 * Created by Amit.Kumar2 on 12/23/2018.
 */
public class ThreadController {


    public List<Future<RemoteWebDriver>> spawnNodeThreads(Integer num, String browserName) throws ExecutionException, InterruptedException {

        String nodeName;
        Worker worker;
        ExecutorService executor = Executors.newFixedThreadPool(num);
        List<Future<RemoteWebDriver>> remotes = new ArrayList<>();


        for(int i=1; i<=num; i++){
            System.out.println("Number is ....." + i);
            nodeName = "node" + i;
            System.out.println(nodeName + Thread.currentThread().getName());
            worker = new Worker(nodeName, browserName);
            Future<RemoteWebDriver> fut1 = executor.submit(worker);
            System.out.println(fut1 + Thread.currentThread().getName());
            remotes.add(fut1);
            //System.out.println(fut1 + Thread.currentThread().getName());

        }


        remotes.forEach((value)-> {

            while(!value.isDone()){
                try {
                    System.out.println("Waiting for thread" + "   " + "to stop");
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });

//        remotes.forEach((key, value)-> {
//
//            try {
//                value.get().quit();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
//
//        });

        return remotes;


    }

    public Hashtable<String, Future<RemoteWebDriver>> spawnBroserThreads(List<String> browsers) throws ExecutionException, InterruptedException {

        String nodeName;
        Worker worker;
        ExecutorService executor = Executors.newFixedThreadPool(1);
        Hashtable<String, Future<RemoteWebDriver>> remotes = new Hashtable<>();


        for(String browserName : browsers){
            nodeName = browserName;
            System.out.println(nodeName + Thread.currentThread().getName());
            worker = new Worker(nodeName, browserName);
            Future<RemoteWebDriver> fut1 = executor.submit(worker);
            System.out.println(fut1 + Thread.currentThread().getName());
            remotes.put(nodeName, fut1);
            //System.out.println(fut1 + Thread.currentThread().getName());

        }

        remotes.forEach((key, value)-> {

            while(!value.isDone()){
                try {
                    System.out.println("Waiting for thread" + "  " + key + "   " + "to stop");
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });

//        remotes.forEach((key, value)-> {
//
//            try {
//                value.get().quit();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
//
//        });

        return remotes;


    }

//    public static void main(String[] args) throws ExecutionException, InterruptedException {
//
//        ThreadController td = new ThreadController();
//
//        td.spawnNodeThreads(2, "chrome");
//    }


}
