package Velocity;

import org.junit.jupiter.api.extension.*;

public class TestExtension implements ParameterResolver, BeforeAllCallback, AfterAllCallback {

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return false;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return null;
    }

    @Override
    public void afterAll(ExtensionContext extensionContext) throws Exception {

        System.out.println("inside after all test cases " + Thread.currentThread().getName());

    }

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception {

        System.out.println("inside before all test cases" + Thread.currentThread().getName());

    }
}
