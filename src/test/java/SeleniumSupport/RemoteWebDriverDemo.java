package SeleniumSupport;

import io.github.bonigarcia.seljup.DriverCapabilities;
import io.github.bonigarcia.seljup.DriverUrl;
import io.github.bonigarcia.seljup.SeleniumExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.openqa.selenium.remote.DesiredCapabilities.chrome;
import static org.openqa.selenium.remote.DesiredCapabilities.firefox;


@ExtendWith(SeleniumExtension.class)
class RemoteWebDriverDemo {

    @DriverUrl
    String url = "http://172.16.42.53:4444/wd/hub";

    @DriverCapabilities
    Capabilities capabilities = chrome();

    @Test
    void testWithRemoteChrome(RemoteWebDriver driver) {
        exercise(driver);
    }

//    @Test
//    void testWithRemoteFirefox(RemoteWebDriver driver) {
//        exercise(driver);
//    }

    void exercise(WebDriver driver) {
        driver.get("https://bonigarcia.github.io/selenium-jupiter/");
        assertThat(driver.getTitle(),
                containsString("JUnit 5 extension for Selenium"));
    }
}
