package TestNgLauncher;

import BuildInExtensions.TestExecutionListenerExample;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.LoggingListener;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import static org.junit.platform.engine.discovery.ClassNameFilter.includeClassNamePatterns;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;

public class CustomLauncherExample {

    public static void main(String[] args) {

        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(selectPackage("PlayGround"))
                .filters(includeClassNamePatterns(".*Launcher.*"))
                .build();

        Launcher launcher = LauncherFactory.create();

        SummaryGeneratingListener summaryGeneratingListener = new SummaryGeneratingListener();

        LoggingListener logger = LoggingListener.forJavaUtilLogging();

        launcher.registerTestExecutionListeners(new TestExecutionListenerExample());
        launcher.registerTestExecutionListeners(summaryGeneratingListener);
        launcher.registerTestExecutionListeners(logger);

        launcher.execute(request);
        System.out.println(summaryGeneratingListener.getSummary().getContainersSucceededCount());


    }
}
