package SeleniumSupport;

import io.github.bonigarcia.seljup.SeleniumExtension;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


class SeleniumTestDemo {


    @RegisterExtension
    static SeleniumExtension seleniumExtension = new SeleniumExtension();


    @Test
    void testWithOneChrome(ChromeDriver chromeDriver) {
        chromeDriver.get("https://www.google.com");
    }

//    @Test
//    void testWithFirefox(FirefoxDriver firefoxDriver) {
//       firefoxDriver.get("https://www.google.com");
//    }
//
//    @Test
//    void testWithChromeAndFirefox(ChromeDriver chromeDriver,
//                                         FirefoxDriver firefoxDriver) {
//        // Use Chrome and Firefox in this test
//    }

    
}
