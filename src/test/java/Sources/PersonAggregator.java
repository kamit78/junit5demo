package Sources;

import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.aggregator.ArgumentsAggregationException;
import org.junit.jupiter.params.aggregator.ArgumentsAggregator;

public class PersonAggregator implements ArgumentsAggregator {
    @Override
    public Person aggregateArguments(ArgumentsAccessor argumentsAccessor, ParameterContext parameterContext) throws ArgumentsAggregationException {

        Person p = new Person();
        p.setFirName(argumentsAccessor.getString(0));
        p.setLastName(argumentsAccessor.getString(1));
        p.setGender(argumentsAccessor.getString(3));
        p.setDateOfBirth(argumentsAccessor.getString(3));
        return p;
    }
}
