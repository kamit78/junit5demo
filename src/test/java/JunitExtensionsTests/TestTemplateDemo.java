package JunitExtensionsTests;

import BuildInExtensions.TestTemplateInvocationContextDemo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(TestTemplateInvocationContextDemo.class)
class TestTemplateDemo {

    final List<String> fruits = Arrays.asList("apple", "banana", "lemon");

    @TestTemplate
    void testTemaplates(String fruit){

        assertTrue(fruits.contains(fruit));

    }

}