package JunitExtensionsTests;

import BuildInExtensions.ParameterResolverDemo;
import BuildInExtensions.ResolverClass;
import BuildInExtensions.TestWatcherDemo;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ParameterResolverDemo.class)
class InjectParameter {

    @Test
    void injectParamenterTest(ResolverClass story){

        System.out.println(story.getFirstName());
        System.out.println(story.getLastName());
    }
}
