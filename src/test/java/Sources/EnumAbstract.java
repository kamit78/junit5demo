package Sources;

public enum  EnumAbstract {

    HIGH {
        @Override
        public String asLowerCase(){
            return HIGH.toString().toLowerCase();
        }
    };

    public abstract String asLowerCase();
}
