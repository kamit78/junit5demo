package Utilities;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;

/**
 * Created by Amit.Kumar2 on 12/23/2018.
 */
public class Worker implements Callable<RemoteWebDriver> {


    private String nodeName;
    private String browserName;

    Worker(String nodeName, String browserName) {
        this.nodeName = nodeName;
        this.browserName = browserName;
        System.out.println(nodeName + "   "  + browserName);
    }

    @Override
    public RemoteWebDriver call() throws Exception {
        System.out.println("inside" + Thread.currentThread().getName());
        DesiredCapabilities cap = new DesiredCapabilities();
        RemoteWebDriver driver;
        cap.setBrowserName(browserName);
        cap.setCapability("id", nodeName);
        System.out.println("finish setting capability" + Thread.currentThread().getName());

        try {
            System.out.println("instantiating driver....");
            driver = new RemoteWebDriver(new URL("http://172.16.42.53:4444/wd/hub"), cap);
            System.out.println("returning driver .............." + Thread.currentThread().getName());
            return driver;

        } catch (MalformedURLException e) {
            System.out.println("Exception occurred .....");
            e.printStackTrace();
            return null;
        }
    }
}
