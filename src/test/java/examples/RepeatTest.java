package examples;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

class RepeatTest {


    @RepeatedTest(value = 10, name = "{currentRepetition} von {totalRepetitions}")
    void repeastIt(TestInfo testInfo){

        System.out.println("Repeating test ...." + testInfo.getDisplayName());
    }
}
