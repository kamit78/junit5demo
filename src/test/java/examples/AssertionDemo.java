package examples;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.OS;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Amit.Kumar2 on 2/20/2019.
 */

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AssertionDemo {


    private static String greetings(){

        return "greetings from junit 5";
    }

    @Tag("maven")
    @Test
    void standardAssertion(){

        assertEquals(4, 4 , ()-> "Assert to equate two values");

        assertTrue('a' < 'b', ()-> "Assertion messages can be lazily evaluated -- \"+\"to avoid constructing complex messages unnecessarily.");

    }

    @Tag("maven")
    @Test
    void groupedAssertions(){

        assertAll("person",
                ()-> assertEquals(1,1),
                () -> assertEquals(2,2)
        );

//        assertTrue(false, ()-> "test passess");
    }

    @DisabledOnOs({OS.WINDOWS})
    @Test
    void dependentAssertions(){

        String firstName = "amit";
        String lastName = "kumar";

        assertAll("properties", ()-> {

                    assertNotNull(firstName);

                    // Executed only if the previous assertion is valid.
                    assertAll("first Name",
                            () -> assertTrue(firstName.startsWith("a")),
                            () -> assertTrue(firstName.endsWith("t"))
                    );
                },
                () -> {

                    // Grouped assertion, so processed independently
                    // of results of first name assertions.
                    System.out.println("Runnint assertion for last name");
                    assertNotNull(lastName);

                    assertAll("last Name",
                            () -> assertTrue(lastName.startsWith("k")),
                            () -> assertTrue(lastName.endsWith("r"))
                    );
                },
                ()-> {

                    System.out.println("Runnnig equal assertion....");

                    assertEquals("hello","hello");

                }
        );

        System.out.println("helllo");
    }

    @Order(1)
    @Test
    void ExceptionTesting(){

        Exception exception = assertThrows(ArithmeticException.class, ()-> {

            System.out.println(1/0);

        });

        System.out.println(exception.getMessage());
    }

    @Test
    void TimeOutNotExceeded(){

        assertTimeout(Duration.ofSeconds(2), ()-> {

            System.out.println("perform task that take time less than 2");
            Thread.sleep(3000);

        });

    }


    @Test
    void timeoutNotExceededWithMethod(){

        String actualGreetings = assertTimeout(Duration.ofSeconds(2), AssertionDemo::greetings);

        System.out.println(actualGreetings);

    }

}
