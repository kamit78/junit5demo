package PlayGround;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class TestSuiteBeforeCallbacks implements BeforeAllCallback, AfterAllCallback {


    private ExtensionContext.Namespace NAMESPACE = ExtensionContext.Namespace.create("test");

    @Override
    public void afterAll(ExtensionContext extensionContext) throws Exception {

        System.out.println(extensionContext.getRoot());

    }

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception {



    }
}
