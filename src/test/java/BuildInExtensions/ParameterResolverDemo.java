package BuildInExtensions;

import org.junit.jupiter.api.extension.*;

import java.lang.reflect.Parameter;

public class ParameterResolverDemo implements ParameterResolver, BeforeTestExecutionCallback {


    private static final ExtensionContext.Namespace NAMESPACE = ExtensionContext.Namespace.create(ParameterResolverDemo.class);


    public ParameterResolverDemo() {
        super();
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {

        Parameter parameter = parameterContext.getParameter();

        return ResolverClass.class.equals(parameter.getType());

    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {

        Class<?> clazz = extensionContext.getRequiredTestClass();

        return extensionContext.getStore(NAMESPACE).get(clazz.getName());

    }

    @Override
    public void beforeTestExecution(ExtensionContext extensionContext) throws Exception {

        ResolverClass story = new ResolverClass();
        story.setFirstName("Amit");
        story.setLastName("Kumar");

        extensionContext.getStore(NAMESPACE).put(extensionContext.getRequiredTestClass().getName(), story);

    }
}
