package BuildInExtensions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import static org.junit.jupiter.api.Assertions.*;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;

class TemporaryFile {

    @Test
    void writeListtoFile(@TempDir Path tempDir)throws IOException {

        Path file = tempDir.resolve("test.txt");
//        System.out.println(file.toString());
        assertTrue(Files.exists(file));
    }

}
