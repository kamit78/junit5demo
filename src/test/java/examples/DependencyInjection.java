package examples;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

@DisplayName("Test Info Demo")
public class DependencyInjection {

    DependencyInjection(TestInfo testInfo){

        System.out.println(testInfo.getTags());

    }

    @Tag("Hello")
    @Test
    void test01(TestInfo testInfo){

        System.out.println(testInfo.getDisplayName());

    }
}
