package PlayGround;

import BuildInExtensions.TestExecutionListenerExample;
import BuildInExtensions.TestWatcherDemo;
import Velocity.TestExtension;
import Velocity.VelocityExecution;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.platform.engine.DiscoverySelector;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import static org.junit.jupiter.api.Assertions.*;

//@ExtendWith(VelocityExecution.class)
class Script1Parallel {

    @Order(1)
    @Tag("maven")
    @Test
    void test01(RemoteWebDriver driver) {
        Launcher launcher = LauncherFactory.create();
        launcher.registerTestExecutionListeners(new TestExecutionListenerExample());

        System.out.println("test 1 of sript 1" + Thread.currentThread().getName());
//        driver.get("https://www.google.com");
//        fail("Test fails.......");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Order(2)
    @Tag("maven")
    @Test
    void test02(RemoteWebDriver driver) {
        System.out.println("test 2 of sript 1" + Thread.currentThread().getName());
//        driver.get("https://www.google.com");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
