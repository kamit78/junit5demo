package JunitExtensionsTests;

import BuildInExtensions.TestWatcherDemo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(TestWatcherDemo.class)
class TestWatcherTest {

    @Test
    void test01(TestInfo testInfo) {

        System.out.println("hello");
        System.out.println(testInfo.getTestMethod().get().getDeclaringClass().getName());

    }
}
