package examples;

import Sources.Book;
import Sources.Person;
import Sources.PersonAggregator;
import Sources.TimeUnitEnum;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.AggregateWith;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.*;
import org.junit.platform.commons.util.StringUtils;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;

class ParametrizedTest {

    @ParameterizedTest
    @ValueSource(strings = { "racecar", "radar", "able was I ere I saw elba" })
    void palindromes(String candidate) {
        assertTrue(StringUtils.isNotBlank(candidate));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = { " ", " ", "\t", "\n" })
    void nullEmptyAndBlankStrings(String text) {
        System.out.println(text);
        assertTrue(text == null || text.trim().isEmpty());
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"h", "j"})
    void test01(String text){

        assertTrue(text==null || text.equals("h") || text.equals("j") );
    }

    @ParameterizedTest
    @EnumSource(TimeUnitEnum.class)
    void testWithEnumSource(TimeUnitEnum sources){

        assertNotNull(sources);

    }

    @ParameterizedTest
    @EnumSource(value = TimeUnitEnum.class, names = {"DAYS", "HOUR"})
    void testWithEnumSource2(TimeUnitEnum sources){

        assertNotNull(sources);

    }

    @ParameterizedTest
    @EnumSource(value = TimeUnitEnum.class, mode= EXCLUDE, names = { "DAYS", "HOUR" })
    void testWithEnumSourceExclude(TimeUnitEnum timeUnit) {
        assertFalse(EnumSet.of(TimeUnitEnum.DAYS, TimeUnitEnum.HOUR).contains(timeUnit));
//        assertTrue(timeUnit.name().length() > 5);
    }

    @ParameterizedTest
    @MethodSource("stringProvider")
    void testWithExplicitLocalMethodSource(String argument) {
        assertNotNull(argument);
    }

    static Stream<String> stringProvider() {
        return Stream.of("apple", "banana");
    }

//    If you do not explicitly provide a factory method name via @MethodSource, JUnit Jupiter will search
//    for a factory method that has the same name as the current @ParameterizedTest method by
//    convention. This is demonstrated in the following example.

    @ParameterizedTest
    @MethodSource
    void testWithDefaultLocalMethodSource(String argument) {
        assertNotNull(argument);
    }


    static Stream<String> testWithDefaultLocalMethodSource() {
        return Stream.of("apple", "banana");
    }


    //In case of multiple arguments
    @ParameterizedTest
    @MethodSource("stringIntAndListProvider")
    void testWithMultiArgMethodSource(String str, int num, List<String> list) {
        assertEquals(5, str.length());
        assertTrue(num >=1 && num <=2);
        assertEquals(2, list.size());
    }




    static Stream<Arguments> stringIntAndListProvider() {
        return Stream.of(
                arguments("apple", 1, Arrays.asList("a", "b")),
                arguments("lemon", 2, Arrays.asList("x", "y"))
        );

    }



    @ParameterizedTest
    @CsvFileSource(resources = "/junitdata.csv", numLinesToSkip = 0)
    void testWithCsvFileSource(int country, int reference) {
        assertNotEquals(0, reference);
        System.out.println(country);
        System.out.println(reference);

    }


    @ParameterizedTest
    @ValueSource(strings = "42 Cats")
    void testWithImplicitFallbackArgumentConversion(Book book) {
        assertEquals("42 Cats", book.getTitle());
    }

//    By default, each argument provided to a @ParameterizedTest method corresponds to a single method
//    parameter. Consequently, argument sources which are expected to supply a large number of
//    arguments can lead to large method signatures.

    @ParameterizedTest
    @CsvSource({
            "Jane, Doe, F, 1990-05-20",
            "John, Doe, M, 1990-10-22"
    })
    void testWithArgumentsAccessor(ArgumentsAccessor arguments){

        System.out.println(arguments.getString(0));
        System.out.println(arguments.getString(1));
        System.out.println(arguments.getString(2));
        System.out.println(arguments.getString(3));

    }

//    Apart from direct access to a @ParameterizedTest method’s arguments using an ArgumentsAccessor,
//    JUnit Jupiter also supports the usage of custom, reusable aggregators.

    @ParameterizedTest
    @CsvSource({

            "Jane, Doe, F, 1990-05-20",
            "John, Doe, M, 1990-10-22"
    })
    void testWithArgumentsAggregator(@AggregateWith(PersonAggregator.class)Person person) {

        System.out.println(person.getFirName());
        System.out.println(person.getLastName());
        System.out.println(person.getGender());
        System.out.println(person.getDateOfBirth());
    }




}
